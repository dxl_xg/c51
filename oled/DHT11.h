#ifndef _DHT11_H_
#define _DHT11_H_

#include "reg52.h"
#include<intrins.h>

#define uchar unsigned char
#define uint unsigned int

sbit Data=P1^6;   //定义数据线
extern uchar rec_dat[9];   //用于显示的接收数据数组

void DHT11_delay_us(uchar n);
void DHT11_delay_ms(uint z);
void DHT11_start();
uchar DHT11_rec_byte();      //接收一个字节
void DHT11_receive();      //接收40位的数据
void UsartInit();	//9600



#endif

