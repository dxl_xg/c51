#ifndef __KEY_H_
#define __KEY_H_

#include<reg52.h>

#include"i2c.h"
#include"ds1302.h"

sbit k2=P1^0;  //时间减1
sbit k3=P1^2;  //时间块加1
sbit k4=P3^3;  //模式选择
sbit k1=P1^1;  //时间加1
sbit k5=P1^3;  //时间块减1
extern signed char status;	 //时间
extern signed char A_flag;	 //闹钟

extern signed char hour;
extern signed char minute;
extern signed char second;

extern unsigned char m;   //秒记数，满100为1秒
extern unsigned char n;
extern unsigned char pp;

extern  uchar flag;
extern  uchar fleg;

void key1();   //时间加1
void key2();   //时间减1
void key3();   //时间块加1
void key4();   //时间块减1
void inint1();
void int1();
void inint0();
void int0();
void time0_Init();
void time0();
int CaculateWeekDay(int y,int m, int d);

void Alarm_key3();   //闹钟时间块加1
void Alarm_key4();   //闹钟时间块减1
void Alarm_key1();   //闹钟时间加1
void Alarm_key2();   //闹钟时间减1

void time_on();	  //开启定时器
void time_off();	  //关闭定时器,暂停
void time_res();  //复位定时器


#endif

