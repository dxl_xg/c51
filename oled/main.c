#include "REG52.h"
#include "i2c.h"
#include "DHT11.h"
#include "DS1302.h"
#include "key.h"

uchar rec_dat[9];   //用于显示的接收数据数组
//
//uchar ALARM[3];

sbit led=P2^7;

uint i,j=0;
uchar  singletime[13];	 //

signed char hour=7;
signed char minute=40;
signed char second=30;

uchar m,n=0;
uchar pp=0;

void display()
{	 
     //time_change();
   	 singletime[0]=TIME[6]/16;//年的十位
	 singletime[1]=TIME[6]%16;//年的个位
	 singletime[2]=TIME[4]/16;//月的十位
	 singletime[3]=TIME[4]%16;//月的个位
	 singletime[4]=TIME[3]/16;//日的十位
	 singletime[5]=TIME[3]%16;//日的个位
	 singletime[6]=TIME[2]/16;//时的十位，24时制
	 singletime[7]=TIME[2]%16;//时的个位
	 singletime[8]=TIME[1]/16;//分的十位
	 singletime[9]=TIME[1]%16;//分的个位
	 singletime[10]=TIME[0]/16;//秒的十位
	 singletime[11]=TIME[0]%16;//秒的个位
//	 TIME[5]=CaculateWeekDay(TIME[6],TIME[4],TIME[3]);
	 singletime[12]=TIME[5]%16;//星期几
	  
}

void Delay1ms(int t)		//@12.000MHz
{
	int a,b;
	for(;t>0;t--)
	{
		for(a=199;a>0;a--)
		{
			for(b=1;b>0;b--)
			{
			 	;
			}
		}
	}
}

void AlarmClock(uchar h,uchar m,uchar s)   //闹钟
{
	if(singletime[6]==h/10)
	{
		if(singletime[7]==h%10)
		{
			if(singletime[8]==m/10)
			{
				if(singletime[9]==m%10)
				{
					if(singletime[10]==s/10)
					{
						if(singletime[11]>=s%10 && singletime[11]<=5+(s%10))
						{
							led=1;
						}
						else
							led=0;
					}
				}
			}
		}
	}
}

void main()
{
   	led=0;

	oled_initial();	  //oled初始化
//	if(Ds1302Read(0xc1)!=(0x20/16)*10+(0x20%16))
		Ds1302Init();    //DS1302初始化
	inint1();   //外部中断初始化
	inint0(); 
	time0_Init();  //定时器初始化
	oled_clear();  //清屏

//	oled_chineseshow(4,8,2,1,4);	 //显示开机

	DHT11_delay_ms(1500);    //DHT11上电后要等待1S以越过不稳定状态在此期间不能发送任何指令
	while(1)
	{
    	DHT11_receive();
//	 	p=rec_dat;

		
/************************温湿度串口调试*************************/
//		for(j=0;j<9;j++)
//		{
//			SBUF = *p;
//			while(!TI)                   //如果发送完毕，硬件会置位TI
//			{
//				_nop_();	
//			}
//			p++;
//			if(*p == '\0') break;		//在每个字符串的最后，会有一个'\0'
//			TI = 0;					  //TI清零
//		}

		if(flag==1)	  //进入时间调节模式
		{
			oled_shownum(0,0,1,0,17);  //显示2
			oled_shownum(1,0,1,0,15);  //显示0
			oled_chineseshow(2,0,1,1,0); //显示年
			oled_chineseshow(4,0,1,1,1); //显示月
			oled_chineseshow(6,0,1,1,2); //显示日
			oled_chineseshow(6,1,1,1,3);  //显示周
		
			oled_shownum(2,2,1,0,25);	  //显示时分之间的 :
			oled_shownum(5,2,1,0,25);	  //显示分秒之间的 :
		
			oled_shownum(11,6,1,0,51);	  //温度 T
			oled_shownum(11,7,1,0,39);	  //湿度 H
			oled_shownum(12,6,1,0,25);	  //:
			oled_shownum(12,7,1,0,25);	  //:
		
			oled_shownum(15,6,1,0,4);	 //%
			oled_shownum(15,7,1,0,95);   //C
	
		  	oled_shownum(13,6,1,0,rec_dat[0]-33);	//湿度十位
			oled_shownum(14,6,1,0,rec_dat[1]-33);	//湿度个位
			oled_shownum(13,7,1,0,rec_dat[6]-33);	//温度十位
			oled_shownum(14,7,1,0,rec_dat[7]-33);	//温度个位

			key1();
			key2();
			key3();
			key4();
			time_change();
		    display();

		 	oled_shownumy(2,0,1,0, singletime[0]);//显示年十位
			oled_shownumy(3,0,1,0, singletime[1]);//显示年个位
	
			oled_shownumy(6,0,1,0, singletime[2]);//显示月十位
			oled_shownumy(7,0,1,0, singletime[3]);//显示月个位
	
			oled_shownumy(10,0,1,0, singletime[4]);//显示日十位
			oled_shownumy(11,0,1,0, singletime[5]);//显示日个位
	
//			oled_chineseshow(4,2,1,1,10+singletime[12]);	 //显示周几
			oled_zhoushow(7,1,1,1,singletime[12]-1);	 //显示周几

			oled_shownumy(0,2,1,0, singletime[6]);//显示时十位
			oled_shownumy(1,2,1,0, singletime[7]);//显示时个位
	
			oled_shownumy(3,2,1,0, singletime[8]);//显示分十位
			oled_shownumy(4,2,1,0, singletime[9]);//显示分个位
	
			oled_shownumy(6,2,1,0, singletime[10]);//显示秒十位
			oled_shownumy(7,2,1,0, singletime[11]);//显示秒个位				
		}
		if(flag==0)	 //正常显示
		{
			i=0;
			j=0;
			oled_shownum(0,0,1,0,17);  //显示2
			oled_shownum(1,0,1,0,15);  //显示0
			oled_chineseshow(2,0,1,1,0); //显示年
			oled_chineseshow(4,0,1,1,1); //显示月
			oled_chineseshow(6,0,1,1,2); //显示日
			oled_chineseshow(6,1,1,1,3);  //显示周
		
			oled_shownum(2,2,1,0,25);	  //显示时分之间的 :
			oled_shownum(5,2,1,0,25);	  //显示分秒之间的 :
		
			oled_shownum(11,6,1,0,51);	  //温度 T
			oled_shownum(11,7,1,0,39);	  //湿度 H
			oled_shownum(12,6,1,0,25);	  //:
			oled_shownum(12,7,1,0,25);	  //:
		
			oled_shownum(15,6,1,0,4);	 //%
			oled_shownum(15,7,1,0,95);   //C
	
		  	oled_shownum(13,6,1,0,rec_dat[0]-33);	//湿度十位
			oled_shownum(14,6,1,0,rec_dat[1]-33);	//湿度个位
			oled_shownum(13,7,1,0,rec_dat[6]-33);	//温度十位
			oled_shownum(14,7,1,0,rec_dat[7]-33);	//温度个位

			Ds1302ReadTime();
			time_change();
		    display();
		 	oled_shownum(2,0,1,0, singletime[0]+15);//显示年十位
			oled_shownum(3,0,1,0, singletime[1]+15);//显示年个位
	
			oled_shownum(6,0,1,0, singletime[2]+15);//显示月十位
			oled_shownum(7,0,1,0, singletime[3]+15);//显示月个位
	
			oled_shownum(10,0,1,0, singletime[4]+15);//显示日十位
			oled_shownum(11,0,1,0, singletime[5]+15);//显示日个位
	
//			oled_chineseshow(4,2,1,1,10+singletime[12]+15);	 //显示周几
			oled_zhoushow(7,1,1,1,singletime[12]-1);	 //显示周几

			oled_shownum(0,2,1,0, singletime[6]+15);//显示时十位
			oled_shownum(1,2,1,0, singletime[7]+15);//显示时个位
	
			oled_shownum(3,2,1,0, singletime[8]+15);//显示分十位
			oled_shownum(4,2,1,0, singletime[9]+15);//显示分个位
	
			oled_shownum(6,2,1,0, singletime[10]+15);//显示秒十位
			oled_shownum(7,2,1,0, singletime[11]+15);//显示秒个位
	
			AlarmClock(hour,minute,second);	  // 闹钟
		}
		if(flag==2)		 //闹钟调节模式
		{
			if(i==0) oled_clear();  //清屏
			i++;

			oled_shownum(2,2,1,0,25);	  //显示时分之间的 :
			oled_shownum(5,2,1,0,25);	  //显示分秒之间的 :
			Alarm_key3();   //闹钟时间块加1
			Alarm_key4();   //闹钟时间块减1
			Alarm_key1();   //闹钟时间加1
			Alarm_key2();   //闹钟时间减1

			oled_shownum(0,2,1,0,(hour/10)+15);//显示时十位
			oled_shownum(1,2,1,0,(hour%10)+15);//显示时个位
	
			oled_shownum(3,2,1,0,(minute/10)+15);//显示分十位
			oled_shownum(4,2,1,0,(minute%10)+15);//显示分个位

			oled_shownum(6,2,1,0,(second/10)+15);//显示秒十位
			oled_shownum(7,2,1,0,(second%10)+15);//显示秒个位	
		}
		
		if(flag==3)  //秒表模式
		{
			if(j==0) 
			{
				oled_clear();  //清屏
//				oled_shownum(5,2,1,0,25);	  //显示秒与毫秒之间的 :
			}
			j++;

			time_on();	  //开启定时器
			time_off();	  //关闭定时器,暂停
			time_res();  //复位定时器

			if(pp>=1)	//10ms时间到	 1*10ms=10ms，中断程序中pp++
			{	
				pp=0;	 //pp清0，等待下一次循环计数
				m++; 	//m是秒计时
		        if(m>10)	  //计数10后，满1秒钟，m清0
				{
					m=0;
					n++;   
					if(n>99)   //100秒
					{
						n=0;
					}	   
				}
			}				

		

			oled_shownum(3,2,1,0,(n/10)+15);//显示秒十位
			oled_shownum(4,2,1,0,(n%10)+15);//显示秒个位
//			oled_shownum(6,2,1,0,(m/10)+15);//显示毫秒十位
//			oled_shownum(7,2,1,0,(m%10)+15);//显示毫秒个位	
		}		                
  	 }	 
}